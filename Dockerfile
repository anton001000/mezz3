FROM python:3.8

RUN apt update
RUN mkdir /srv/project
WORKDIR /srv/project

COPY ./src ./src
COPY ./commands ./commands

RUN pip install -r ./src/mezz/requirements.txt

CMD ["bash"]